import sqlite3

connection = sqlite3.connect('database.db')


with open('schema.sql') as f:
    connection.executescript(f.read())

cur = connection.cursor()

cur.execute("INSERT INTO books (title, author ,content) VALUES (?,?,?)",
            ('Essentialism: The Disciplined Pursuit of Less', 'Greg McKeown', 'Content for the first post')
            )

connection.commit()
connection.close()
