import json
import os
import sqlite3

from flask import Flask, render_template, jsonify, request, url_for, flash, redirect,abort
from crypto.mian import get_prices_from_binance
# from fake_NoSQL.controler import get_data

app = Flask(__name__)


def get_db_connection():
    conn = sqlite3.connect('database.db')
    conn.row_factory = sqlite3.Row
    return conn

def get_book(book_id):
    conn = get_db_connection()
    book = conn.execute('SELECT * FROM books WHERE id = ?',
                        (book_id,)).fetchone()
    conn.close()
    if book is None:
        abort(404)
    return book

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/tools/prices')
def prices():
    # last_prices = get_last_prices()
    prices = get_prices_from_binance()
    return render_template('tools/prices_template.html', prices=prices)

@app.route('/tools/todo')
def todo():
    return render_template('tools')
@app.route('/tools/diet')
def diet():
    return render_template('diet')

@app.route('/crypto/referrals')
def referrals():
    # with open("favicon.ico/data.json", "r") as f:
    #     json_data = json.load(f)

    ref_data = [
        {"name": "Binance",
         "description": "Binance stands as a global powerhouse in the cryptocurrency exchange market, offering an extensive range of trading options for digital assets, derivatives, and token sales. Renowned for its robust trading platform, comprehensive financial tools, and competitive fees, Binance is a preferred choice for both novice and experienced traders looking to engage in the dynamic world of crypto trading.",
         "url": "https://www.binance.com/en/activity/referral-entry/CPA?ref=CPA_00CFHRLB3N"},
        {"name": "Bitvavo",
         "description": "Bitvavo offers a streamlined and user-friendly platform tailored for European users, facilitating the buying, selling, and trading of a wide variety of digital currencies. With its low fees, easy-to-navigate interface, and commitment to security, Bitvavo is designed to democratize access to cryptocurrency trading, making it accessible to investors at all levels.",
         "url": "https://bitvavo.com/invite?a=6FA3B76D1F"},
        {"name": "OKX",
         "description": "OKX, previously known as OKEx, operates as a versatile cryptocurrency exchange and trading platform, featuring a rich ecosystem that includes futures, spot trading, and a DeFi suite. Catering to a global audience, OKX is celebrated for its innovative trading solutions, robust security measures, and a comprehensive suite of crypto-related services aimed at empowering users to navigate the crypto markets efficiently.",
         "url": "https://www.okx.com/join/38550101"},
        {"name": "KRAKEN",
         "description": "Kraken is a stalwart in the cryptocurrency exchange sector, esteemed for its security-first approach, deep liquidity, and wide array of supported cryptocurrencies. Offering advanced trading features, fiat currency support, and detailed market insights, Kraken serves as a trusted platform for traders and investors seeking to explore and expand their cryptocurrency portfolios.",
         "url": "https://kraken.app.link/Vohwb2CRRGb"},
        {"name": "Revolut",
         "description": "Revolut revolutionizes banking with its digital-first approach, offering a versatile app for managing personal finances, including currency exchange, budgeting tools, and cryptocurrency trading. With a focus on innovation and user experience, Revolut provides a seamless solution for modern financial management, catering to a global audience looking for more than just traditional banking services.",
         "url": "https://revolut.com/referral/?referral-code=maurychsmi!FEB1-24-AR-L2"},
    ]
    # ref_data = json_data["data"]


    # print(get_data())
    return render_template('referrals.html', ref_data=ref_data)

# hobbies
@app.route("/hobbies/books")
def books():
    conn = get_db_connection()
    books = conn.execute('SELECT * FROM books').fetchall()
    conn.close()
    return render_template('pages/hobbies/books.html', books=books)

@app.route('/hobbies/movies')
def movies():
    return render_template('movies.html')

@app.route('/hobbies/music')
def music():
    return render_template('music.html')

#admin 

from admin import admin as admin_blueprint
app.register_blueprint(admin_blueprint)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=int("2137"), debug=True)
