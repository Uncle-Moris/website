import requests

symbols = [
        'BTCUSDT',
        'ETHUSDT',
        'SOLUSDT',
        'DOGEUSDT',
        'ICPUSDT'
]

def get_prices_from_binance(symbols=symbols):
    prices=[]
    for symbol in symbols:
        price = requests.get(f'https://api.binance.com/api/v3/ticker/price?symbol={symbol}')
        prices.append(price.json())
    return prices

prices = get_prices_from_binance()
print(prices)

for p in prices:
    print(p)
