cd /opt/website
last_commit_hash=$(cat last_hash.txt)

git pull origin

current_commit_hash=$(git rev-parse HEAD)

if [ "$current_commit_hash" != "$last_commit_hash" ];then
	pip3 install -r requirements.txt 
	systemctl restart website.service
	echo $current_commit_hash > last_hash.txt 
fi

