from flask import Blueprint

admin = Blueprint('admin', __name__)

@admin.route('/admin')
def admin_main():
    return "<h1>Hello</h1>"

@admin.route('/admin/books/add', methods=('GET', 'POST'))
def add_book():
    if request.method == 'POST':
        title = request.form['title']
        author = request.form['author']
        content = request.form['content']

        if not title:
            flash('Title is required!')
        elif not content:
            flash('Content is required!')
        else:
            conn = get_db_connection()
            conn.execute('INSERT INTO books (title, author,content) VALUES (?,?,?)',
                         (title, author, content))
            conn.commit()
            conn.close()
            return redirect(url_for('books'))

    return render_template('admin/books/create.html')


@admin.route('/admin/books/<int:id>/edit/', methods=('GET', 'POST'))
def edit_book(id):
    book = get_book(id)

    if request.method == 'POST':
        title = request.form['title']
        author = request.form['author']
        content = request.form['content']

        if not title:
            flash('Title is required!')

        elif not content:
            flash('Content is required!')

        elif not author:
            flash('author is required!')
        else:
            conn = get_db_connection()
            conn.execute('UPDATE books SET title = ?, author = ? ,content = ?'
                         ' WHERE id = ?',
                         (title, author, content, id))
            conn.commit()
            conn.close()
            return redirect(url_for('books'))

    return render_template('admin/books/edit.html', book=book)

@admin.route('/admin/books/<int:id>/delete/', methods=('POST',))
def delete(id):
    book = get_book(id)
    conn = get_db_connection()
    conn.execute('DELETE FROM books WHERE id = ?', (id,))
    conn.commit()
    conn.close()
    flash('"{}" was successfully deleted!'.format(book['title']))
    return redirect(url_for('books'))
