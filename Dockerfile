FROM python:3.10.13-alpine3.19
WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt
EXPOSE 2137
CMD python ./app.py

